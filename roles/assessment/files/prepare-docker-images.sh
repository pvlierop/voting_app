cd /root

eval $(minikube docker-env)

docker image build --label result-app --tag result-app:v1 result-app/

docker image build --label worker --tag worker:v1 worker/

docker image build --label voting-app --tag voting-app:v1 voting-app/

docker pull postgres:9.4

docker pull redis:alpine
