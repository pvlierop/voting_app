## DevOps assessment

Welcome to the git repository of the voting app. This part of the assessment done for Merapar.

The development and testing is done on Debian 9.3.

### Prerequisites
* Install Debian 9.3 on hardware or a virtual machine.

_When using a virtual machine make sure to make the virtual extensions available to the vm.
eg. for KVM this is called [nested KVM](https://www.server-world.info/en/note?os=CentOS_7&p=kvm&f=7)_

* Login as root user

* Install ansible and git.

```
~# apt-get install ansible git
```
* Clone this git repository
```
~# git clone https://pvlierop@bitbucket.org/pvlierop/voting_app.git
```
* Change dir to the voting app directory
```
~# cd voting_app
```

### Starting the cluster

To start the cluster for bringing up Minikube with the containers and their services use the folowing command.

```
~# ansible-playbook -c local start_cluster.yml
```
After a few moments the cluster will be running.
This ansible playbook can be run repeatly. If for some reason something fails  the playbook can be run again.

### Viewing the voting and result app

The voting and the result app can be viewed on {NodeIP}:30080 resp {NodeIP}:30090.
Or by running the following commands:
```
~# minikube service voting-app

~# minikube service result-app
```

